import random
import requests
import time
import os
import pathlib

UNAUTH_COUNTRY = "US"


def check_my_ip():
    print('checking your IP')
    try:
        response = requests.get('https://api.ipify.org')
        if response.status_code == 200:
            print(f'My IP: {response.text}')
            print("--DONE IP--")
            return response.text
        else:
            return None
    except:
        print("error with checking IP")
        return None
    
def check_country(IP):
    print('checking country of IP')
    query = ' https://ipapi.co/' + IP + '/country/'
    try:
        response = requests.get(query)
        print(f'My Country: {response.text}')
        print("--DONE Country--")
        return response.text
    except:
        print("error with checking country - REQUEST.get module")
        raise
    
def safe_check(my_country, UNAUTH_COUNTRY):
    if my_country == UNAUTH_COUNTRY:
        print(f'NOT_SAFE: Your Source Country: {my_country} = unauthorized origin of {UNAUTH_COUNTRY}')
        return "NOT_SAFE"
    else:
        return "SAFE"
    
def get_vpn_list():
    config_files_location = pathlib.Path.cwd() / 'hesm/configs/'
    vpn_list = list(config_files_location.glob("*.ovpn"))
    print(vpn_list)
    return vpn_list

def rotate_vpn(vpn_list):
    print("rotating VPN connections")
    return str(vpn_list[random.randrange(0, len(vpn_list))])
    
def get_vpn():
    vpn = rotate_vpn(get_vpn_list())
    print(vpn)
    return vpn

def is_vpn_running():
    return True

def connect_vpn(config_file):
    print("Connecting VPN")
    cmd = f'openvpn --config {config_file} --auth-user-pass auth.txt '
    print(cmd)
    
def disconnect_vpn():
    print("Disconnecting VPN")
    
def kill_vpn():
    print("killing VPN service")
            
    
#######
config_file = get_vpn()
status = True

while status == True:
    my_ip = check_my_ip()
    if my_ip != None:
        my_country = check_country(my_ip)
        status = safe_check(my_country, UNAUTH_COUNTRY)
        if status == "NOT_SAFE":
            print('Not Safe: doing VPN Stuff')
            vpn_service = False
            connect_vpn(config_file)
            time.sleep(10)
        else:
            print("doing nothing, your safe")
            time.sleep(10)
    else:
        status == False
    
