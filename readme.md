#Purpose

Used to check country of origin host is coming from. If the host is coming from an undesirable country, it will connect to a predetermine list of VPN connections.

#Steps

1. Load vpn ovpn files into VPN folder. Currently script is set to run nordvpn config files, located: https://nordvpn.com/ovpn/
2. Edit the auth.txt, first line should be your NordVPN username, second line NordVPN Password, script is hardcoded to run --auth-user-pass auth.txt. Will probably work with any other vpn provider that has an ovpn config file.
3. run python HESM.py

#run
Currently script is run through crontab or manually
